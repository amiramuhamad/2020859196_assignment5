function validateForm(){
	var length= document.forms["Form"]["length"].value;
	var width= document.forms["Form"]["width"].value;
	var height= document.forms["Form"]["height"].value;
	var mode= document.forms["Form"]["mode"].value;
	var type= document.forms["Form"]["type"].value;
	var name= document.forms["Form"]["name"].value;
	var weight=(length * width * height)/5000;
	
	alert("Parcel Volumetric and Cost Calculator"
		+"\nCustomer name: "+name
		+"\nLength: "+ length + " cm"
		+"\nWidth: "+ width + " cm"
		+"\nHeight: "+ height + " cm"
        +"\nWeight: "+ weight + " kg"
		+"\nMode: "+ mode
        +"\nType: "+ type
        +"\nDelivery cost: RM "+calcCost(weight));
}

function resetAlert(){
var reset= document.forms["Form"]["reset"].value;
	alert("The input will be reset");
}

function upperCase(){
 	var name= document.forms["Form"]["name"];	
 	 name.value = name.value.toUpperCase();
 }

function calcCost(weight){
	var mode= document.forms["Form"]["mode"].value;
	var type= document.forms["Form"]["type"].value;
	var cost;
	if (type=="Domestic"){
		if(weight<2){
			if(mode=="Surface"){
				cost= 7;
			}
			else if (mode=="Air"){
				cost=10;
			}
		}
		else if(weight>=2){
			if(mode=="Surface"){
				cost=7+((weight-2)*1.5);
			}
			else if (mode=="Air"){
				cost=10+((weight-2)*3);
			}
		}
	}
	else if(type=="International"){
		if(weight<2){
			if(mode=="Surface"){
				cost=20;
			}
			else if (mode=="Air"){
				cost=50;
			}
		}
		else if(weight>=2){
			if(mode=="Surface"){
				cost=20+((weight-2)*3);
			}
			else if (mode=="Air"){
				cost=50+((weight-2)*5);
			}
		}
	} 	
	return cost;
	
}
